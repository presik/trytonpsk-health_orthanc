
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class ImagingRequest(metaclass=PoolMeta):
    __name__ = "health.imaging.request"
    studies = fields.One2Many("health.orthanc.study", "imaging_request",
        "Studies", readonly=True
    )

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({"do_sync": {
            'invisible': Eval('state') == 'done',
        }})

    @classmethod
    @ModelView.button
    def do_sync(cls, records):
        cls.sync(records)

    @classmethod
    def sync(cls, records=None):
        """Sync Orthanc studies with this model"""
        for rec in records:
            pass
