
from trytond.model import fields
from trytond.pool import PoolMeta


class Patient(metaclass=PoolMeta):
    __name__ = "health.patient"
    orthanc_patients = fields.One2Many(
        "health.orthanc.patient", "patient", "Orthanc patients"
    )
