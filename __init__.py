
from trytond.pool import Pool
from . import orthanc
from . import health
from . import imaging
from . import wizard
from . import ir


def register():
    Pool.register(
        wizard.AddOrthancInit,
        wizard.AddOrthancResult,
        orthanc.OrthancServerConfig,
        orthanc.OrthancStudy,
        orthanc.OrthancPatient,
        health.Patient,
        imaging.ImagingRequest,
        ir.Cron,
        module="health_orthanc", type_="model",
    )
    Pool.register(
        wizard.FullSyncOrthanc,
        module="health_orthanc", type_="wizard")
