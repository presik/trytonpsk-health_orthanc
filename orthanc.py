
from trytond.model import ModelView, ModelSQL, fields, Unique
from trytond.pool import Pool
from trytond.transaction import Transaction
from beren import Orthanc as RestClient
from requests.auth import HTTPBasicAuth as auth
from datetime import datetime
from urllib.parse import urljoin
import logging
import pendulum

logger = logging.getLogger(__name__)


class OrthancServerConfig(ModelSQL, ModelView):
    """Orthanc server details"""
    __name__ = "health.orthanc.config"
    _rec_name = "label"

    label = fields.Char("Label", required=True, help="Label for server (eg., remote1)")
    domain = fields.Char("URL", required=True,
        help="The full URL of the Orthanc server"
    )
    user = fields.Char("Username", required=True,
        help="Username for Orthanc REST server"
    )
    password = fields.Char("Password", required=True,
        help="Password for Orthanc REST server"
    )
    last = fields.BigInteger("Last Index", readonly=True, help="Index of last change")
    sync_time = fields.DateTime("Sync Time", readonly=True,
        help="Time of last server sync"
    )
    validated = fields.Boolean(
        "Validated", help="Whether the server details have been successfully checked"
    )
    since_sync = fields.Function(
        fields.TimeDelta("Since last sync", help="Time since last sync"),
        "get_since_sync",
    )
    since_sync_readable = fields.Function(
        fields.Char("Since last sync", help="Time since last sync"),
        "get_since_sync_readable",
    )
    patients = fields.One2Many("health.orthanc.patient", "server", "Patients")
    studies = fields.One2Many("health.orthanc.study", "server", "Studies")
    link = fields.Function(
        fields.Char("URL", help="Link to server in Orthanc Explorer"), "get_link"
    )

    def get_link(self, name):
        pre = "".join([self.domain.rstrip("/"), "/"])
        add = "app/explorer.html"
        return urljoin(pre, add)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ("label_unique", Unique(t, t.label),
            "The label must be unique.")
        ]
        cls._buttons.update({"do_sync": {}})

    @classmethod
    @ModelView.button
    def do_sync(cls, servers):
        cls.sync(servers)
        cls.update_image_request()

    @classmethod
    def sync(cls, servers=None):
        """Sync from changes endpoint"""

        pool = Pool()
        patient = pool.get("health.orthanc.patient")
        study = pool.get("health.orthanc.study")

        if not servers:
            servers = cls.search([("domain", "!=", None), ("validated", "=", True)])

        logger.info("Starting sync")
        print("Starting sync..........")
        for server in servers:
            if not server.validated:
                continue
            logger.info("Getting new changes for <{}>".format(server.label))
            orthanc = RestClient(server.domain, auth=auth(server.user, server.password))
            curr = server.last
            new_patients = set()
            update_patients = set()
            new_studies = set()
            update_studies = set()

            while True:
                try:
                    changes = orthanc.get_changes(since=curr)
                except:
                    server.validated = False
                    logger.exception("Invalid details for <{}>".format(server.label))
                    break
                for change in changes["Changes"]:
                    type_ = change["ChangeType"]
                    if type_ == "NewStudy":
                        new_studies.add(change["ID"])
                    elif type_ == "StableStudy":
                        update_studies.add(change["ID"])
                    elif type_ == "NewPatient":
                        new_patients.add(change["ID"])
                    elif type_ == "StablePatient":
                        update_patients.add(change["ID"])
                    else:
                        pass
                curr = changes["Last"]
                if changes["Done"] is True:
                    logger.info("<{}> at newest change".format(server.label))
                    break

            update_patients -= new_patients
            update_studies -= new_studies
            if new_patients:
                patient.create_patients(
                    [orthanc.get_patient(p) for p in new_patients], server
                )
            if update_patients:
                patient.update_patients(
                    [orthanc.get_patient(p) for p in update_patients], server
                )
            if new_studies:
                study.create_studies(
                    [orthanc.get_study(s) for s in new_studies], server
                )
            if update_studies:
                study.update_studies(
                    [orthanc.get_study(s) for s in update_studies], server
                )
            server.last = curr
            server.sync_time = datetime.now()
            logger.info(
                "<{}> sync complete: {} new patients, {} update patients, {} new studies, {} updated studies".format(
                    server.label,
                    len(new_patients),
                    len(update_patients),
                    len(new_studies),
                    len(update_studies),
                )
            )
        cls.save(servers)

    @classmethod
    def update_image_request(cls):
        pool = Pool()
        ImageRequest = pool.get("health.imaging.request")
        Study = pool.get("health.orthanc.study")
        requests = ImageRequest.search([
            ('state', 'in', ['requested', 'in_analysis'])
        ])
        for request in requests:
            studies = Study.search([
                ('number', '=', request.number)
            ])
            for study in studies:
                study.imaging_request = request.id
                study.save()

    @staticmethod
    def quick_check(domain, user, password):
        """Validate the server details"""

        try:
            orthanc = RestClient(domain, auth=auth(user, password))
            orthanc.get_changes(last=True)
        except:
            return False
        else:
            return True

    @fields.depends("domain", "user", "password")
    def on_change_with_validated(self):
        return self.quick_check(self.domain, self.user, self.password)

    def get_since_sync(self, name):
        return datetime.now() - self.sync_time

    def get_since_sync_readable(self, name):
        try:
            d = pendulum.now() - pendulum.instance(self.sync_time)
            return d.in_words(Transaction().language)
        except:
            return ""


class OrthancPatient(ModelSQL, ModelView):
    """Orthanc patient information"""
    __name__ = "health.orthanc.patient"

    patient = fields.Many2One("health.patient", "Patient",
        help="Local linked patient")
    name = fields.Char("PatientName", readonly=True)
    bd = fields.Date("Birthdate", readonly=True)
    patient_id = fields.Char("PatientID", readonly=True, help="Equivalent to MRN (Medical Record Number)")
    uuid = fields.Char("PatientUUID", readonly=True, required=True)
    studies = fields.One2Many(
        "health.orthanc.study", "patient", "Studies", readonly=True
    )
    server = fields.Many2One("health.orthanc.config", "Server", readonly=True)
    link = fields.Function(
        fields.Char("URL", help="Link to patient in Orthanc Explorer"), "get_link"
    )

    def get_link(self, name):
        pre = "".join([self.server.domain.rstrip("/"), "/"])
        add = "app/explorer.html#patient?uuid={}".format(self.uuid)
        return urljoin(pre, add)

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints = [(
            "uuid_unique",
            Unique(t, t.server, t.uuid),
            "UUID must be unique for a given server",
        )]

    @staticmethod
    def get_info_from_dicom(patients):
        """Extract information for writing to database"""

        data = []
        for patient in patients:
            tags = patient.get("MainDicomTags")
            try:
                bd = datetime.strptime(tags["PatientBirthDate"], "%Y%m%d").date()
            except:
                bd = None

            patientName = tags.get("PatientName")
            patientName = patientName.replace('^', ' ')
            data.append({
                "name": patientName.upper(),
                "bd": bd,
                "patient_id": tags.get("PatientID"),
                "uuid": patient.get("ID"),
            })
        return data

    @classmethod
    def update_patients(cls, patients, server):
        """Update patients"""

        Patient = Pool().get("health.patient")

        entries = cls.get_info_from_dicom(patients)
        updates = []
        for entry in entries:
            try:
                patient = cls.search(
                    [("uuid", "=", entry["uuid"]), ("server", "=", server)], limit=1
                )[0]
                patient.name = entry["name"]
                patient.bd = entry["bd"]
                patient.patient_id = entry["patient_id"]
                if not patient.patient:  # don't update unless no patient attached
                    try:
                        g_patient = Patient.search(
                            [("puid", "=", entry["patient_id"])], limit=1
                        )[0]
                        patient.patient = g_patient
                        logger.info(
                            "New Matching PUID found for {}".format(entry["patient_id"])
                        )
                    except:
                        pass
                updates.append(patient)
                logger.info("Updating patient {}".format(entry["uuid"]))
            except:
                continue
                logger.warning("Unable to update patient {}".format(entry["uuid"]))
        cls.save(updates)

    @classmethod
    def create_patients(cls, patients, server):
        """Create patients"""

        pool = Pool()
        Patient = pool.get("health.patient")

        entries = cls.get_info_from_dicom(patients)
        for entry in entries:
            try:
                g_patient = Patient.search([("puid", "=", entry["patient_id"])], limit=1)[0]
                logger.info("Matching PUID found for {}".format(entry["uuid"]))
            except:
                g_patient = None
            entry["server"] = server
            entry["patient"] = g_patient
        cls.create(entries)


class OrthancStudy(ModelSQL, ModelView):
    """Orthanc study"""
    __name__ = "health.orthanc.study"

    imaging_request = fields.Many2One("health.imaging.request", "Img. Resquest")
    patient = fields.Many2One("health.orthanc.patient", "Patient", readonly=True)
    uuid = fields.Char("UUID", readonly=True, required=True)
    description = fields.Char("Description", readonly=True)
    number = fields.Char("Numder", readonly=True)
    date = fields.Date("Date", readonly=True)
    instance_uid = fields.Char("StudyInstanceUID", readonly=True)
    patient_id = fields.Char("PatientID", readonly=True, help="MRN (Medical Record Number)")
    institution = fields.Char("Institution", readonly=True,
        help="Imaging center where study was undertaken")
    radiologist = fields.Char("Referring Physician", readonly=True)
    physician = fields.Char("Requesting Physician", readonly=True)
    server = fields.Many2One("health.orthanc.config", "Server", readonly=True)
    link = fields.Function(
        fields.Char("URL", help="Link to study in Orthanc Explorer"), "get_link"
    )
    viewer_link = fields.Function(
        fields.Char("Viewer", help="Link to study image"), "get_viewer_link"
    )

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints = [(
            "uuid_unique",
            Unique(t, t.server, t.uuid),
            "UUID must be unique for a given server",
        )]

    def get_link(self, name):
        pre = "".join([self.server.domain.rstrip("/"), "/"])
        add = f"app/explorer.html#study?uuid={self.uuid}"
        return urljoin(pre, add)

    def get_viewer_link(self, name):
        try:
            # Remove port
            protocol, url_base, _ = self.server.domain.split(':')
            domain = protocol + ':' + url_base
            pre = "".join([domain.rstrip("/"), "/"])
            add = f"viewer?StudyInstanceUIDs={self.instance_uid}"
            return urljoin(pre, add)
        except Exception as e:
            print('e')

    def get_rec_name(self, name):
        return ": ".join((self.uuid, str(self.date)))

    @staticmethod
    def get_info_from_dicom(studies):
        """Extract information for writing to database"""

        data = []
        for study in studies:
            tags = study.get("MainDicomTags")
            patient_tags = study.get("PatientMainDicomTags")
            try:
                date = datetime.strptime(tags["StudyDate"], "%Y%m%d").date()
            except:
                date = None
            try:
                description = tags["RequestedProcedureDescription"]
            except:
                description = tags.get("StudyDescription", '')
            print('study....', study)
            data.append({
                "parent_patient": study["ParentPatient"],
                "uuid": study["ID"],
                "patient_id": patient_tags.get("PatientID"),
                "number": tags.get("AccessionNumber"),
                "description": description,
                "date": date,
                "instance_uid": tags.get('StudyInstanceUID'),
                "institution": tags.get("InstitutionName"),
                "radiologist": tags.get("ReferringPhysicianName"),
                "physician": tags.get("RequestingPhysician"),
            })
        return data

    @classmethod
    def update_studies(cls, studies, server):
        """Update studies"""

        entries = cls.get_info_from_dicom(studies)
        updates = []
        for entry in entries:
            try:
                study = cls.search(
                    [("uuid", "=", entry["uuid"]), ("server", "=", server)], limit=1
                )[0]
                study.description = entry["description"]
                study.date = entry["date"]
                # study.patient_id = entry["patient_id"]
                study.institution = entry["institution"]
                study.radiologist = entry["radiologist"]
                study.physician = entry["physician"]
                updates.append(study)
                logger.info("Updating study {}".format(entry["uuid"]))
            except:
                continue
                logger.warning("Unable to update study {}".format(entry["uuid"]))
        cls.save(updates)

    @classmethod
    def create_studies(cls, studies, server):
        """Create studies"""

        pool = Pool()
        Patient = pool.get("health.orthanc.patient")

        entries = cls.get_info_from_dicom(studies)
        for entry in entries:
            try:
                patient = Patient.search(
                    [("uuid", "=", entry["parent_patient"]), ("server", "=", server)],
                    limit=1,
                )[0]
            except:
                patient = None
                logger.warning(
                    "No parent patient found for study {}".format(entry["ID"])
                )
            entry.pop("parent_patient")  # remove non-model entry
            entry["server"] = server
            entry["patient"] = patient
        cls.create(entries)
